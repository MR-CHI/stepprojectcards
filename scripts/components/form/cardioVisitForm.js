import {Form} from "./form.js";
import {Input} from "../input/input.js";
import {Textarea} from "../textarea/textarea.js";
import {request} from "../axios/axios.js";
import {VisitCardiologist} from "../card/visitCardiologist.js";

export class CardioVisitForm extends Form {
    submitProps = {
        type: 'submit',
        value: 'Create visit'
    }
    person = {
        type: 'text',
        name: "user-name",
        placeholder: 'Фамилия Имя Отчество'
    }
    agePerson = {
        type: 'text',
        name: 'user-age',
        placeholder: 'Возраст'
    }
    pressure = {
        type: 'text',
        name: 'user-pressure',
        placeholder: 'Обычное давление'
    }
    bodyMassIndex = {
        type: 'text',
        name: 'user-body',
        placeholder: 'Индекс массы тела'
    }
    pastIllnesses = {
        type: 'text',
        name: 'user-illness',
        placeholder: 'Перенесенные заболевания сердечно-сосудистой системы'
    }
    purposeVisit = {
        type: 'text',
        name: 'visit-purpose',
        placeholder: 'Цель визита'
    }
    descriptionProps = {
        name: 'user-description',
        placeholder: 'Краткое описание визита'
    }

    formProps = {
        className: 'form cardio-form'
    }

    render() {
        const {person, agePerson, purposeVisit, pressure, bodyMassIndex, pastIllnesses, formProps, descriptionProps} = this;
        const inputName = new Input(person).render();
        const inputAge = new Input(agePerson).render();
        const inputPurpose = new Input(purposeVisit).render();
        const inputPressure = new Input(pressure).render();
        const inputBodyMassIndex = new Input(bodyMassIndex).render();
        const inputPastIllnesses = new Input(pastIllnesses).render();
        const textDescription = new Textarea(descriptionProps).render();
        const formObj = new Form(formProps);
        const form = formObj.render();
        const submit = new Input(this.submitProps)
        const select = document.createElement('select');
        select.className = 'selectCardio'
        const optionDefault = new Option('Введите срочность')
        const optionCommon = new Option('Обычная')
        const optionPriority = new Option('Приоритетная')
        const optionUrgent = new Option('Неотложная')
        select.append(optionDefault, optionCommon, optionPriority, optionUrgent)
        form.append(select, inputName, inputAge, inputPurpose, inputPressure, inputBodyMassIndex, inputPastIllnesses, textDescription, submit.render());

        form.addEventListener('submit', function (e) {
            e.preventDefault()
            if (!inputName.value || !inputAge.value || !inputPurpose.value || !inputPressure.value || !inputBodyMassIndex.value || !inputPastIllnesses.value || !textDescription.value || select.value === 'Введите срочность') {
                const err = document.createElement("p")
                err.className = 'form-error'
                err.textContent = 'all fields should be filled in'
                form.append(err)
            } else {
                const selectDoc = document.querySelector('.select-doctor')
                const body = {
                    doctor: selectDoc.value,
                    person: inputName.value,
                    pressure: inputPressure.value,
                    birthday: inputAge.value,
                    bodyMassIndex: inputBodyMassIndex.value,
                    pastIllnesses: inputPastIllnesses.value,
                    purpose: inputPurpose.value,
                    description: textDescription.value,
                    selectPriority: select.value,
                }
                CardioVisitForm.body = body
                request.post('/cards', body).then(({data}) => {
                    const cardObj = new VisitCardiologist()
                    const card = cardObj.render()
                    card.setAttribute('data-id', data.id)
                    const container = document.querySelector('.cards')
                    container.append(card)
                    form.remove()
                    const modal = document.querySelector('.add-visit-modal')
                    modal.remove()
                })
            }
        })
        return form
    }
}