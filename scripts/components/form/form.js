import {Component} from "../component.js";

export class Form extends Component {

    render() {
        const {funcSubmit, submitType, ...attr} = this.props
        const form = this.createElement('form', attr)
        this.form = form
        return form
    }

    serialize() {
        const fields = [...this.form.querySelectorAll('input[name], select[name], textarea[name]')]
        const body = fields.map((item) => `${item.name}=${item.value}`).join('&')
        return body
    }

    serializeJSON() {
        const fields = [...this.form.querySelectorAll('input[name], select[name], textarea[name]')];
        const body = fields.map(({name, value}) => ({[name]: value}))
        return JSON.stringify(body)
    }
}