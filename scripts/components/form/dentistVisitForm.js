import {Form} from "./form.js";
import {Input} from "../input/input.js";
import {Textarea} from "../textarea/textarea.js";
import {request} from "../axios/axios.js";
import {VisitDentist} from "../card/visitDentist.js";

export class DentistVisitForm extends Form {
    submitProps = {
        type: 'submit',
        value: 'Create visit'
    }
    person = {
        type: 'text',
        name: "user-name",
        placeholder: 'Фамилия Имя Отчество'
    }
    descriptionProps = {
        name: 'user-description',
        placeholder: 'Краткое описание визита'
    }
    dataLastVisit = {
        type: 'text',
        name: 'visit-data',
        placeholder: 'Дата последнего визита'
    }
    purposeVisit = {
        type: 'text',
        name: 'visit-purpose',
        placeholder: 'Цель визита'
    }
    formProps = {
        className: 'form dentist-form'
    }

    render() {
        const {person, descriptionProps, purposeVisit, formProps, dataLastVisit} = this;
        const inputName = new Input(person).render()
        const inputPurpose = new Input(purposeVisit).render()
        const inputDataVisit = new Input(dataLastVisit).render()
        const textarea = new Textarea(descriptionProps).render()
        const formObj = new Form(formProps)
        const form = formObj.render()
        const submit = new Input(this.submitProps)
        const select = document.createElement('select')
        select.className = 'selectPre'
        this.select = select
        const optionDefault = new Option('Введите срочность')
        const optionCommon = new Option('Обычная')
        const optionPriority = new Option('Приоритетная')
        const optionUrgent = new Option('Неотложная')
        select.append(optionDefault, optionCommon, optionPriority, optionUrgent)
        form.append(select, inputName, inputPurpose, inputDataVisit, textarea, submit.render());
        form.addEventListener('submit', function (e) {
            e.preventDefault()
            if (!inputName.value || !inputPurpose.value || !inputDataVisit.value || !textarea.value || select.value === 'Введите срочность') {
                const err = document.createElement("p")
                err.className = 'form-error'
                err.textContent = 'all fields should be filled in'
                form.append(err)
            } else {
                const selectDoc = document.querySelector('.select-doctor')
                const body = {
                    doctor: selectDoc.value,
                    person: inputName.value,
                    purpose: inputPurpose.value,
                    date: inputDataVisit.value,
                    description: textarea.value,
                    selectPriority: select.value
                }
                DentistVisitForm.body = body
                request.post('/cards', body).then(({data}) => {
                    const cardObj = new VisitDentist()
                    const card = cardObj.render()
                    card.setAttribute('data-id', data.id)
                    const container = document.querySelector('.cards')
                    container.append(card)
                    form.remove()
                    const modal = document.querySelector('.add-visit-modal')
                    modal.remove()
                })
            }
        })
        return form
    }
}
