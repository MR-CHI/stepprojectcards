import {Modal} from "./modal.js";
import {LoginForm} from "../form/loginForm.js";

export class LoginModal extends Modal {
    loginFormProps ={
        className: 'login-form'
    }
    loginModalProps = {
        className: 'modal',
    }
    render(){
        const  loginModal = super.renderModal(this.loginModalProps)
        const loginForm = new LoginForm(this.loginFormProps)
        loginModal.append(loginForm.render())
        return loginModal
    }
}