import {Visit} from "./visit.js";
import {DentistVisitForm} from "../form/dentistVisitForm.js";
import {ModalAddVisit} from "../modal/modalAddVisit.js";
import {request} from "../axios/axios.js";

export class VisitDentist extends Visit {
    visitProps = DentistVisitForm.body

    render() {
        const empty = document.querySelector('.empty-text')
        if(empty){
            empty.remove()
        }
        const {doctor, person, purpose, description, selectPriority, date} = this.visitProps
        const card = super.render()
        card.insertAdjacentHTML('afterbegin', `<h3 class="doctor">Доктор : ${doctor}</h3>
                                                            <p class="patient-name">ФИО пациента: ${person}</p>
                                                            <p class="hidden-info priority">Приоритет ${selectPriority}</p>
                                                            <p class="hidden-info purpose">Цель визита: ${purpose}</p>
                                                            <p class="hidden-info last-visit">Дата последнего визита: ${date}</p>
                                                            <p class="hidden-info description">Краткое описание: ${description}</p>`)
        const changeBtn = card.querySelector('.change-card-btn')
        changeBtn.addEventListener('click', function () {
            const id = card.getAttribute('data-id')
            const modalObj = new ModalAddVisit()
            const modal = modalObj.render()
            modal.innerHTML = `<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                 </button>`
            const closeBtn = modal.querySelector('.close')
            closeBtn.addEventListener('click', function(){
                modal.remove()
            })
            request.get(`/cards/${id}`).then(({data}) => {
                const formObj = new DentistVisitForm()
                formObj.submitProps.value = 'Change'
                formObj.person.value = data.person
                formObj.dataLastVisit.value = data.data
                formObj.purposeVisit.value = data.purpose
                formObj.descriptionProps.value = data.description
                const form = formObj.render()
                const select = form.querySelector('.selectPre')
                const option = [...select.querySelectorAll('option')]
                const selected = data.selectPriority
                option.forEach(el =>{
                    if(el.value.includes(selected)){
                        el.setAttribute('selected', 'true')
                    }
                })
                form.addEventListener('submit', function (e) {
                    e.preventDefault()
                    const person = this.querySelector('[name="user-name"]').value
                    const purpose = this.querySelector('[name="visit-purpose"]').value
                    const date = this.querySelector('[name="visit-data"]').value
                    const description = this.querySelector('[name="user-description"]').value
                    const selectPriority = this.querySelector('.selectPre').value
                    const body = {
                        doctor: 'dentist',
                        person, purpose, date, description, selectPriority
                    }
                    formObj.body = body
                    request.put(`/cards/${id}`, body).then(({data}) => {
                        const {person, selectPriority, purpose, date, description} = data
                        const patient = card.querySelector('.patient-name')
                        patient.textContent = `ФИО пациента: ${person}`
                        const priority = card.querySelector('.priority')
                        priority.textContent = `Приоритет ${selectPriority}`
                        const purposeEl = card.querySelector('.purpose')
                        purposeEl.textContent = `Цель визита: ${purpose}`
                        const visit = card.querySelector('.last-visit')
                        visit.textContent = `Дата последнего визита: ${date}`
                        const descriptionEl = card.querySelector('.description')
                        descriptionEl.textContent = `Краткое описание: ${description}`
                        form.remove()
                        modal.remove()
                    })
                })
                modal.append(form)
                document.body.append(modal)
            })
        })
        return card
    }
}

