import {Button} from "./button.js";
import {request} from "../axios/axios.js";
import {VisitDentist} from "../card/visitDentist.js";
import {VisitCardiologist} from "../card/visitCardiologist.js";
import {VisitTherapist} from "../card/visitTherapist.js";

export class ResetBtn extends Button {
    resetBtnProps = {
        className: 'reset-btn',
        content: 'Reset'
    }

    render() {
        const btn = new Button(this.resetBtnProps)
        const el = btn.render()
        const container = document.querySelector('.cards')
        el.addEventListener('click', function () {
            const cards = document.querySelectorAll('.card-wrapper')
            cards.forEach(el =>el.remove())
            request.get('./cards').then(({data}) => {
                data.forEach(el => {
                    if (el.doctor === 'dentist') {
                        const newVisit = new VisitDentist()
                        newVisit.visitProps = el
                        const card = newVisit.render()
                        container.append(card)
                        card.setAttribute('data-id', el.id)
                        const closeBtn = card.querySelector('.close')
                        closeBtn.addEventListener('click', function () {
                            request.delete(`/cards/${el.id}`).then(({data}) => console.log(data))
                        })
                    }
                    if (el.doctor === 'cardiologist') {
                        const newVisit = new VisitCardiologist()
                        newVisit.visitProps = el
                        const card = newVisit.render()
                        container.append(card)
                        card.setAttribute('data-id', el.id)
                        const closeBtn = card.querySelector('.close')
                        closeBtn.addEventListener('click', function () {
                            request.delete(`/cards/${el.id}`).then(({data}) => console.log(data))
                        })
                    }
                    if (el.doctor === 'therapist') {
                        const newVisit = new VisitTherapist()
                        newVisit.visitProps = el
                        const card = newVisit.render()
                        container.append(card)
                        card.setAttribute('data-id', el.id)
                        const closeBtn = card.querySelector('.close')
                        closeBtn.addEventListener('click', function () {
                            request.delete(`/cards/${el.id}`).then(({data}) => console.log(data))
                        })
                    }
                })
            })
        })
        return el
    }
}