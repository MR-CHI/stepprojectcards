import {Component} from "../component.js";

export class Button extends Component{
    render (){
        const {content, ...attr} = this.props
        const button = this.createElement('button', attr, content)
        button.addEventListener('click', function(e){
            e.preventDefault()
        })
        this.elem = button
        return button
    }
}