import {Button} from "./button.js";
import {request} from "../axios/axios.js";
import {VisitDentist} from "../card/visitDentist.js";
import {VisitCardiologist} from "../card/visitCardiologist.js";
import {VisitTherapist} from "../card/visitTherapist.js";

export class FilterBtn extends Button {
    filterBtnProps = {
        className: 'filter-btn',
        content: 'Filter'
    }

    render() {
        const btn = new Button(this.filterBtnProps)
        const elem = btn.render()
        elem.addEventListener('click', function () {
            const header = document.querySelector('.headers')
            const cardsContainer = document.querySelector('.cards')
            const cards = cardsContainer.querySelectorAll('.card-wrapper')
            const doctor = header.querySelector('.select-doctor-filter').value
            const priority = header.querySelector('.select-priority-filter').value
            if (doctor === 'Select doctor' && priority === 'Select priority') {
                const err = document.createElement('p')
                err.className = 'filter-error'
                err.textContent = 'You should choose something'
                elem.append(err)
            } else {
                const err = document.querySelector('.filter-error')
                if (err) {
                    err.remove()
                }
                cards.forEach(card => card.remove())
                request.get('/cards').then(({data}) => {
                    data.forEach(el => {
                        if ((el.doctor === doctor && priority === 'Select priority') || (el.selectPriority === priority && doctor === 'Select doctor')) {

                            if (el.doctor === 'dentist') {
                                const cardObj = new VisitDentist()
                                cardObj.visitProps = el
                                const card = cardObj.render()
                                card.setAttribute('data-id', el.id)
                                cardsContainer.append(card)
                            }
                            if (el.doctor === 'cardiologist') {
                                const cardObj = new VisitCardiologist()
                                cardObj.visitProps = el
                                const card = cardObj.render()
                                card.setAttribute('data-id', el.id)
                                cardsContainer.append(card)
                            }
                            if (el.doctor === 'therapist') {
                                const cardObj = new VisitTherapist()
                                cardObj.visitProps = el
                                const card = cardObj.render()
                                card.setAttribute('data-id', el.id)
                                cardsContainer.append(card)
                            }
                        }
                        if (el.doctor === doctor && el.selectPriority === priority) {
                            if (el.doctor === 'dentist') {
                                const cardObj = new VisitDentist()
                                cardObj.visitProps = el
                                const card = cardObj.render()
                                card.setAttribute('data-id', el.id)
                                cardsContainer.append(card)
                            }
                            if (el.doctor === 'cardiologist') {
                                const cardObj = new VisitCardiologist()
                                cardObj.visitProps = el
                                const card = cardObj.render()
                                card.setAttribute('data-id', el.id)
                                cardsContainer.append(card)
                            }
                            if (el.doctor === 'therapist') {
                                const cardObj = new VisitTherapist()
                                cardObj.visitProps = el
                                const card = cardObj.render()
                                card.setAttribute('data-id', el.id)
                                cardsContainer.append(card)
                            }
                        }
                    })
                })
            }
        })
        return elem
    }
}