import {LoginModal} from './components/modal/loginModal.js'

const button = document.querySelector('.header-buttons-element')
button.addEventListener('click', function (e) {
    const loginModal = new LoginModal()
    const modal = loginModal.render()
    modal.className = 'modal login-modal'
    document.body.append(modal)
    const close = document.querySelectorAll('.close')
    close.forEach(item => item.addEventListener('click', function () {

            this.parentElement.remove()
        })
    )
    e.preventDefault();
})

const cardContainer = document.querySelector('.cards')
function drop(e) {
    e.preventDefault();
    const elem = document.querySelector("[data-draggable-id=targetObj]");
    cardContainer.append(elem);
    elem.dataset.draggableId = '';
}
function allowDrop(e) {
    e.preventDefault()
}
cardContainer.addEventListener("dragover", allowDrop);
cardContainer.addEventListener("drop", drop);